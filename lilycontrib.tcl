#!/usr/bin/wish

package require Tk

# Helper functions

set lily_dir $env(HOME)/lilypond
if {[file exists $lily_dir]} {
	cd $lily_dir
}

proc write_to_output {s} {
	.output.text insert insert $s
	.output.text see end
}

proc write_file_to_output {f} {
	if {[eof $f]} {
		global git_command
		fconfigure $f -blocking true
		if {[catch {close $f} err]} {
			tk_messageBox -type ok -message "Git aborted: $err"
		}
		unset git_command
	} else {
		write_to_output [read $f 24]
	}
}

proc git {args} {
	global lily_dir git_command
	set git_command [linsert $args 0 "|git" "--git-dir=$lily_dir/.git"]
	set git_command "$git_command 2>@1"
	#.output.text insert end "$git_command\n"
	set git [open $git_command r]
	fconfigure $git -blocking false
	fileevent $git readable [list write_file_to_output $git]
	vwait git_command
}

proc config {args} {
	global lily_dir
	set p [open [linsert $args 0 "|git" --git-dir=$lily_dir/.git config] r]
	set result [regsub "\n\$" [read $p] ""]
	if {[catch {close $p} err]} {
		tk_messageBox -type ok -message "config failed: $err"
	}
	return $result
}

proc config_quiet {args} {
	global lily_dir
	set p [open [linsert $args 0 "|git" --git-dir=$lily_dir/.git config] r]
	set result [regsub "\n\$" [read $p] ""]
	if {[catch {close $p} err]} {
		set result ""
	}
	return $result
}

proc update_lilypond {} {
	global lily_dir
	. config -cursor watch
	if {![file exists $lily_dir]} {
		write_to_output "Cloning LilyPond (this can take some time) ...\n"
		file mkdir $lily_dir
		cd $lily_dir
		git init
		git config core.bare false
		git remote add -t master \
			origin git://repo.or.cz/lilypond.git
		git fetch --depth 1
		git reset --hard origin/master
		git config branch.master.remote origin
		git config branch.master.merge refs/heads/master
		.buttons.update configure -text "Update LilyPond"
	} else {
		global rebase
		write_to_output "Updating LilyPond...\n"
		git fetch origin
		if {$rebase} {
			git rebase origin/master
		} else {
			git merge origin/master
		}
	}
	write_to_output "Done.\n"
	. config -cursor ""
}

proc toggle_rebase {} {
	global rebase
	config --bool branch.master.rebase $rebase
}

# GUI

wm title . "LilyPond Contributor's GUI"

# Buttons

panedwindow .buttons
button .buttons.update -text "Update LilyPond" -command update_lilypond
label .buttons.rebase_label -text "Rebase"
if {![file exists $lily_dir]} {
	.buttons.update configure -text "Clone LilyPond"
}
set rebase 0
if {[config_quiet --bool branch.master.rebase] == true} {
	set rebase 1
}
checkbutton .buttons.rebase -variable rebase -command toggle_rebase
pack .buttons.update -side left
pack .buttons.rebase -side right
pack .buttons.rebase_label -side right

# Output

panedwindow .output
text .output.text -width 80 -height 15 \
	-xscrollcommand [list .output.horizontal set] \
	-yscrollcommand [list .output.vertical set]
scrollbar .output.horizontal -orient h -command [list .output.text xview]
scrollbar .output.vertical -orient v -command [list .output.text yview]
pack .output.horizontal -side bottom -fill x
pack .output.vertical -side right -fill y
pack .output.text -expand true -anchor nw -fill both

pack .buttons .output
